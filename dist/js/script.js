$(document).ready(function(){
	$('.slick__slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    focusOnSelect: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="img/icons_benefits/arrow_prev.png" class="btn_prev"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="img/icons_benefits/arrow_next.png" class="btn_next"></button>'
  });
});

new WOW().init();

/* Joke */

const modalTrigger = document.querySelector('[data-modal]'),
			btnJokeMore = document.querySelector('[data-modal-more]'),
      modal = document.querySelector('.modal'),
			modalJoke = document.querySelector('.modal__joke'),
			joke1 = document.querySelector('.joke1'),
			joke2 = document.querySelector('.joke2'),
      modalClose = document.querySelector('[data-close]');

function closeModalJoke () {
	modal.classList.add('hide');
	modal.classList.remove('show');
	modalJoke.classList.add('hide');
	modalJoke.classList.remove('show');
}

function openModalJoke () {
	modal.classList.add('show', 'fade');
	modal.classList.remove('hide');
	modalJoke.classList.add('show', 'fade');
	modalJoke.classList.remove('hide');
	joke1.classList.add('show', 'fade');
	joke1.classList.remove('hide');
	joke2.classList.add('hide', 'fade');
	joke2.classList.remove('show');
	btnJokeMore.classList.add('show');
	btnJokeMore.classList.remove('hide');
}

modalTrigger.addEventListener('click', () => {
	openModalJoke();
	document.body.style.overflow = 'hidden';
});

modalClose.addEventListener('click', () => {
	closeModalJoke();
	document.body.style.overflow = '';
});

modal.addEventListener('click', (e) => {
	if (e.target === modal) {
		closeModalJoke();
    document.body.style.overflow = '';
	}
});

btnJokeMore.addEventListener('click', () => {
	joke1.classList.add('hide');
	joke1.classList.remove('show');
	joke2.classList.add('show');
	joke2.classList.remove('hide');
	btnJokeMore.classList.add('hide');
});

/* Animations tubs */

const animationBtn = document.querySelectorAll('.animation__btn'), /* tabs */
      animationsBlock = document.querySelector('.display__animations'), /* TabsParent */
      animation = document.querySelectorAll('.display__animation'), /* tabsContent */
      animationArea = document.querySelector('#animation-container'),
      buttonFour = document.querySelector('#animation_4'),
      buttonOne = document.querySelector('#animation_1');


buttonFour.addEventListener('click', () => {
      alert("Если при открытии четвертой анимации у вас пустой экран, то нажмите, пожалуйста, клавишу F12 (для открытия консоли разработчика) и затем сразу закройте открывшееся окошко. После данных манипуляций анимация должна отражаться на экране и реагировать на мышку. Такая проблема связана с необъяснимым поведением JavaScript кода, приносим свои глубочайшие извенения");
}, {once: true}); 


function animationTabs () {
  animationBtn.forEach(function(item) {
    item.addEventListener('click', function () {
      let currentBtn = item;
      let tabId = currentBtn.getAttribute('data-tab');
      let currentTab = document.querySelector(tabId);

      animationBtn.forEach(function(item) {
        item.classList.remove('animation__btn_active');
      });

      animation.forEach(function(item) {
        item.classList.add('hide', 'fade');
      });

      currentBtn.classList.add('animation__btn_active');
      currentTab.classList.toggle('hide');
    });
    
    animationArea.addEventListener('click', (e) => {
      if (e.target === animationArea) {
        animationBtn.forEach(function(item) {
        item.classList.remove('animation__btn_active');

        animation.forEach(function(item) {
        item.classList.add('hide');
      });
      });
      }
    });
  });
}

animationTabs();

/* Timer */

const deadLine = '2021-11-01';

function calculations (endTime) {
  const t = Date.parse(endTime) - Date.parse(new Date()),
        weeks = Math.floor( (t / (1000*60*60*24) / 7) ),
        days = Math.floor( (t / (1000*60*60*24)) ),
        hours = Math.floor( (t / (1000*60*60) % 24) ),
        minutes = Math.floor( (t / (1000*60) % 60) ),
        seconds = Math.floor( (t / 1000) % 60 );

        return {
          'total': t,
          'weeks': weeks,
          'days': days,
          'hours': hours,
          'minutes': minutes,
          'seconds': seconds
        };
}

function getZero (num) {
  if (num >= 0 && num < 10) {
    return '0' + num;
  } else {
    return num;
  }
}

function setClock (selector, endTime) {
  const timer = document.querySelector(selector),
        weeks = document.querySelector('#weeks'),
        days = document.querySelector('#days'),
        hours = document.querySelector('#hours'),
        minutes = document.querySelector('#minutes'),
        seconds = document.querySelector('#seconds'),
        timeInterval = setInterval(updateClock, 1000);

  updateClock();

  function updateClock () {
    const t = calculations(endTime);

    weeks.innerHTML = getZero(t.weeks);
    days.innerHTML = getZero(t.days);
    hours.innerHTML = getZero(t. hours);
    minutes.innerHTML = getZero(t.minutes);
    seconds.innerHTML = getZero(t.seconds);

    if (t.total <= 0) {
      clearInterval(timeInterval);
    }
  }
}

setClock('.timer__item', deadLine);



/* slider */
window.addEventListener('DOMContentLoaded', function() {
  let offsett = 0;
  let slideIndex = 1;

  const slider = document.querySelector('.my__slider'),
        slide = document.querySelectorAll('.my__slider_slide'),
        prev = document.querySelector('.my__slider_prev'),
        next = document.querySelector('.my__slider_next'),
        dots = document.querySelector('.my__slider_dots'),
        dot = document.querySelectorAll('.my__slider_dot'),
        sliderWrapper = document.querySelector('.my__slider_wrapper'),
        sliderField = document.querySelector('.my__slider_inner'),
        width = window.getComputedStyle(sliderWrapper).width,
        sliderBtnBlock = document.querySelector('.slider__button_block'),
        btnSlick = document.querySelector('.btn__slick'),
        btnMySlider = document.querySelector('.btn__mySlider'),
        slickSlider = document.querySelector('.slick__slider'),
        mySlider = document.querySelector('.my__slider');
  

  btnSlick.addEventListener('click', function () {
    btnSlick.classList.toggle('btn__slick_active');
    btnMySlider.classList.remove('btn__mySlider_active');
    slickSlider.classList.add('show', 'fade');
    slickSlider.classList.remove('hide');
    mySlider.classList.remove('show');
    mySlider.classList.add('hide');
  });

  btnMySlider.addEventListener('click', function () {
    btnMySlider.classList.toggle('btn__mySlider_active');
    btnSlick.classList.remove('btn__slick_active');
    slickSlider.classList.add('hide');
    slickSlider.classList.remove('show');
    mySlider.classList.remove('hide');
    mySlider.classList.add('show', 'fade');
  });

  sliderField.style.width = 100 * slide.length + '%';
  sliderField.style.display = 'flex';
  sliderField.style.transition = '0.5s all';

  sliderWrapper.style.overflow = 'hidden';

  slide.forEach(slide => {
    slide.style.width = width;
  });

  slider.style.position = 'relative';

  next.addEventListener('click', () => {
    if (offsett == (+width.slice(0, width.length - 2) * (slide.length - 1))) {
            offsett = 0;
            dot[slideIndex-1].classList.remove('my__slider_dot_active');
        } else {
            offsett += +width.slice(0, width.length - 2); 
        }
    
    sliderField.style.transform = `translateX(-${offsett}px)`;  

    if (slideIndex == slide.length) {
      slideIndex = 1;
    } else {
      slideIndex++;
    }
    
    dot[slideIndex-1].classList.add('my__slider_dot_active');
    dot[slideIndex-2].classList.remove('my__slider_dot_active');
  });

  prev.addEventListener('click', () => {
    if ( offsett == 0) {
            offsett = +width.slice(0, width.length - 2) * (slide.length - 1);
            dot[slideIndex-1].classList.remove('my__slider_dot_active');
        } else {
            offsett -= +width.slice(0, width.length - 2); 
        }
    
    sliderField.style.transform = `translateX(-${offsett}px)`;  

    if (slideIndex == 1) {
      slideIndex = slide.length;
    } else {
      slideIndex--;
    }
    
    dot[slideIndex-1].classList.add('my__slider_dot_active');
    dot[slideIndex].classList.remove('my__slider_dot_active');
  });



  dot.forEach(function (item) {
    item.addEventListener('click', function (e) {
      const slideTo = e.target.getAttribute('data-dot');

      slideIndex = slideTo;
        
      offsett = +width.slice(0, width.length - 2) * (slideTo - 1);
      sliderField.style.transform = `translateX(-${offsett}px)`;
        
        

      dot.forEach(function (dott) {
        dott.classList.remove('my__slider_dot_active');
      });

      if (item.classList.contains('my__slider_dot_active')) {
        item.classList.remove('my__slider_dot_active');
      
      } else {
        item.classList.add('my__slider_dot_active');
      }

    });
  });


});



  /* Catalog */

let tabs = document.querySelectorAll('.catalog__tab'), /* tabs */
    catalogContent = document.querySelectorAll('.catalog__content'),
    linkMore = document.querySelectorAll('.catalog__item_link'), /* TabContent */
    linkBack = document.querySelectorAll('.catalog__item_back'),
    mainPage = document.querySelectorAll('.catalog__item-content'), /* TabsParent */
    moreInfo = document.querySelectorAll('.catalog__item-list'), /* TabsParent */
    tabParent = document.querySelectorAll('.catalogLinks'),
    catalogContentArea = document.querySelectorAll('.catalog__content-area'),
    catalog = document.querySelector('.catalog'),
    catalogItem = document.querySelectorAll('.catalog__item');
    

tabs.forEach((tab) => {
  tab.addEventListener('click', function () {
    let currentTab = tab;
    let tabId = currentTab.getAttribute("data-tab");
    let currentContent = document.querySelector(tabId);
  
    if (!currentTab.classList.contains('catalog__tab_active')) {
      tabs.forEach((item) => {
        item.classList.remove('catalog__tab_active');
      });
     
      catalogContentArea.forEach((item) => {
        item.classList.remove('catalog__content-area_active');
      });

      currentTab.classList.add('catalog__tab_active');
      currentContent.classList.add('catalog__content-area_active', 'fade');
    }
     
    linkMore.forEach((item) => {
        item.addEventListener('click', () => {
          let currentLinkMore = item;
          let mainPageId = currentLinkMore.getAttribute('data-lmore');
          let moreInfoId = currentLinkMore.getAttribute('data-lback');
          let currentMoreInfo = document.querySelector(moreInfoId);
          let currentMainPage = document.querySelector(mainPageId);
          
          
          currentMoreInfo.classList.add('catalog__item-list_active');
          currentMainPage.classList.remove('catalog__item-content_active');

          catalog.addEventListener('click', (e) => {
            if (e.target === catalog) {
              currentMoreInfo.classList.remove('catalog__item-list_active');
              currentMainPage.classList.add('catalog__item-content_active');
            }
          });
        });
    });

    

    linkBack.forEach((item) => {
        item.addEventListener('click', () => {
          let currentLinkBack = item;
          let mainPageId = currentLinkBack.getAttribute('data-lmore');
          let moreInfoId = currentLinkBack.getAttribute('data-lback');
          let currentMoreInfo = document.querySelector(moreInfoId);
          let currentMainPage = document.querySelector(mainPageId);
          
          currentMoreInfo.classList.remove('catalog__item-list_active');
          currentMainPage.classList.add('catalog__item-content_active');

        });
    });
    
  });

});

document.querySelector('.catalog__tab').click();

/* modal catalog img*/
let lookImgBtn = document.querySelectorAll('.catalog__item_btn');
let modalCatalogImg = document.querySelector('.modal__catalog_img');
let closeModalImg = document.querySelector('.modal__catalog_close')

lookImgBtn.forEach(function (item) {
    item.addEventListener('click', () => {
        let currentLookImgBtn = item;
        let currentDataImg = currentLookImgBtn.getAttribute('data-modalImg');
        let currentImg = document.querySelector(currentDataImg);
        let cloneCurrentImg = currentImg.cloneNode(true);

        cloneCurrentImg.style.width = '100%';
        cloneCurrentImg.style.height = '100%';

        modalCatalogImg.append(cloneCurrentImg);

        modal.classList.add('show');
        modal.classList.remove('hide');
        modalCatalogImg.classList.add('show', 'fade2');
        modalCatalogImg.classList.remove('hide');
        document.body.style.overflow = 'hidden';
        
        
        closeModalImg.addEventListener('click', () => {
          modal.classList.add('hide');
          modal.classList.remove('show');
          modalCatalogImg.classList.remove('show');
          modalCatalogImg.classList.add('hide');
          document.body.style.overflow = '';
          cloneCurrentImg.remove(cloneCurrentImg);
        });
        
        modal.addEventListener('click', (e) => {
          if (e.target === modal) {
            modal.classList.add('hide');
            modal.classList.remove('show');
            modalCatalogImg.classList.remove('show');
            modalCatalogImg.classList.add('hide');
            document.body.style.overflow = '';
            cloneCurrentImg.remove(cloneCurrentImg);
          }
        });
    });
});


/* Scroll slow */

let links = document.querySelectorAll('a[href^="#"]'), // все ссылки, с атрибутом href, начинающимся с "#"
    header = document.querySelector('header'), //элемент header, который может быть спозиционирован абслютно или фиксированно
    topOffset = 50;// отступа сверху нет
let upPageBtn = document.querySelector('.up-arrow');
  

links.forEach(item => {
  item.addEventListener('click', function(e) {
	e.preventDefault();
	let href = this.getAttribute('href').slice(1);
	const targetElem = document.getElementById(href);
 
	const elemPosition = targetElem.getBoundingClientRect().top;
	const offsetPosition = elemPosition - topOffset;
	window.scrollBy({
		top: offsetPosition,
		behavior: 'smooth'
	});

  
  /* upPageBtn.addEventListener('click', () => {
      upPageBtn.onclick = function () {
        window.scrollTo(0,0);
      };
      window.scrollBy({
        top: offsetPosition,
        behavior: 'smooth'
      });
      
    }); */
  });
});

upPageBtn.style.opacity = '0';
function magic () {
  if (window.pageYOffset < 1200) {
    upPageBtn.style.opacity = '0';
  } else { upPageBtn.style.opacity = '1'; }
}
//
upPageBtn.onclick = function () {
	window.scrollBy({
		behavior: 'smooth'
	});
};


window.addEventListener('scroll', () => {
  magic();
});  



/* Website */

let websiteBgBtn = document.querySelectorAll('.website__change-btn'),
    website = document.querySelector('.website');

websiteBgBtn.forEach((item) => {
  item.addEventListener('click', (e) => {
    let currentWebBtn = item;
    let currentDataWebBtn = currentWebBtn.getAttribute('data-websiteBg');


    if (currentDataWebBtn == 1) {
      website.style.backgroundImage = 'url(./img/website_bg_1.jpg)';
    } else if (currentDataWebBtn == 2) {
      website.style.backgroundImage = 'url(./img/website_bg_2.jpg)';
    } else if (currentDataWebBtn == 3) {
      website.style.backgroundImage = 'url(./img/website_bg_3.jpg)';
    } else if (currentDataWebBtn == 4) {
      website.style.backgroundImage = 'url(./img/website_bg_4.jpg)';
    }

    if (!currentWebBtn.classList.contains('website__change-btn_active')) {
      websiteBgBtn.forEach((item) => {
        item.classList.remove('website__change-btn_active');
      });
    }

    currentWebBtn.classList.add('website__change-btn_active');
  });
});

websiteBgBtn[0].click();


/* Website search */

let searchBtn = document.querySelector('.website__search'),
    closeSearch = document.querySelector('.website__search-close'),
    searchInput = document.querySelector('.website__search-string');


searchBtn.addEventListener('click', () => {
  searchInput.classList.remove('fade5', 'hide');
  searchInput.classList.add('fade4', 'website__search-string_active');
  searchBtn.classList.add('hide');
  closeSearch.classList.add('show');
  closeSearch.classList.remove('hide');
});

closeSearch.addEventListener('click', () => {
  searchInput.classList.remove('website__search-string_active', 'fade4');
  searchInput.classList.add('fade5'); 
  searchBtn.classList.add('show');
  searchBtn.classList.remove('hide');
  closeSearch.classList.add('hide');
  closeSearch.classList.remove('show');
});


/* Website genres */

let genresBtn = document.querySelector('.website__nav-link'),
    menuParent = document.querySelector('.website__nav'),
    genresSubmenu = document.querySelector('.website__genres'),
    genresSubmenuLink = document.querySelectorAll('.website__genres-link');

function submenu () {

  genresBtn.addEventListener("mouseover", showSub, false);
  genresBtn.addEventListener("mouseout", hideSub, false);
  genresSubmenu.addEventListener("mouseover", showSub, false);
  genresSubmenu.addEventListener("mouseout", hideSub, false);

  /* genresBtn.addEventListener('mouseenter', () => {
    showSub();
  });

  genresBtn.addEventListener('mouseenter', () => {
    hideSub();
  }); */

  function showSub() {
    
    genresSubmenu.style.height = "205px";
    genresSubmenu.style.overflow = "visible";
    genresSubmenu.style.opacity = "1";
    
    
  }


  function hideSub(e) {
      
    genresSubmenu.style.height = "0px";
    genresSubmenu.style.overflow = "hidden";
    genresSubmenu.style.opacity = "0";
    

  }
}

submenu();





/* Website likes */


let like = document.querySelector('.likes__btnG'),
    dislike = document.querySelector('.likes__btnR'),
    likeNomber = document.querySelector('.likes__btnG-counter'),
    dislikeNomber = document.querySelector('.likes__btnR-counter');
    
function likeClick () {
  like.addEventListener('click', () => {
    like.classList.toggle('likes__btnG_active');
    
    if (like.classList.contains('likes__btnG_active')) {
      likeNomber.textContent++;
    } else {
      likeNomber.textContent--;
    }

    if (dislike.classList.contains('likes__btnR_active')) {
      dislike.classList.toggle('likes__btnR_active');
      dislikeNomber.textContent--;
    } 
  });
}

likeClick();

function dislikeClick () {
  dislike.addEventListener('click', () => {
    dislike.classList.toggle('likes__btnR_active');
    
    if (dislike.classList.contains('likes__btnR_active')) {
      dislikeNomber.textContent++;
    } else {
      dislikeNomber.textContent--;
    }

    if (like.classList.contains('likes__btnG_active')) {
      like.classList.toggle('likes__btnG_active');
      likeNomber.textContent--;
    }
  });
}

dislikeClick();


/* Website comments */

let comments = document.querySelector('.website__closeOpenComments'),
    commentsOpenBtn = document.querySelector('.website__comments-open');

commentsOpenBtn.addEventListener('click', () => {
  comments.classList.toggle('hide');
});

let commentsForm = document.querySelector('.website__comments-write'),
    commentText = document.querySelector('.website__comments-textarea'),
    commentName = document.querySelector('.website__comments-name'),
    commentPush = document.querySelector('.website__comments-push'),
    commentWrapper = document.querySelector('.website__comment'),
    comment = document.querySelector('.website__comment-area'),
    commentUserPhoto = document.querySelector('.website__comment-photo'),
    commentUserName = document.querySelector('.website__comment-name'),
    commentUserDate = document.querySelector('.website__comment-date'),
    commentUserText = document.querySelector('.website__comment-text');

commentPush.addEventListener('click', (e) => {
  e.preventDefault();
   let newComment = comment.cloneNode(true);
  if ( !commentName.value || !commentText.value ) {
    newComment.style.display = 'none';
  } else {
   commentUserPhoto.textContent = commentName.value.substr(0, 1).toUpperCase();
   commentUserName.textContent = commentName.value;
   commentUserDate.textContent = new Date().toLocaleString();
   commentUserText.textContent = commentText.value;
   console.log(commentUserName);
   console.log(commentUserText);
   commentWrapper.append(newComment);
  }

});



/* Website cards */

class otherFilms {
    constructor(src, alt, subtitle, year, parent) {
      this.src = src;
      this.alt = alt;
      this.subtitle = subtitle;
      this.year = year;
      this.parent = document.querySelector(parent);
    }

    render() {
      const skeletonCard = document.createElement('div');

        skeletonCard.innerHTML = `
          <div class="website__otherFilms-item">
            <img src=${this.src} alt=${this.alt} class="website__otherFilms-poster">
            <div class="website__otherFilms-subtitle">${this.subtitle}</div>
            <div class="website__otherFilms-year">${this.year}</div>
          </div>
        `;
        this.parent.append(skeletonCard);
    }
}

new otherFilms(
  './img/otherFilms/big_money.jpg',
  'big money',
  'Большой куш',
  '(2001)',
  '.website__otherFilms-content'
).render();

new otherFilms(
  './img/otherFilms/cards_money_two_guns.jpg',
  'cards, money, two guns',
  'Карты, деньги, два ствола',
  '(1998)',
  '.website__otherFilms-content'
).render();

new otherFilms(
  './img/otherFilms/holms_1.jpg',
  'holms 1',
  'Шерлок Холмс',
  '(2009)',
  '.website__otherFilms-content'
).render();

new otherFilms(
  './img/otherFilms/holms_2.jpg',
  'holms 2',
  'Шерлок Холмс: Игра теней',
  '(2011)',
  '.website__otherFilms-content'
).render();

new otherFilms(
  './img/otherFilms/rok.jpg',
  'rok-n-rol',
  'Рок-н-рольщик',
  '(2009)',
  '.website__otherFilms-content'
).render();




/* One animation */

const coffeeName = document.querySelector(".coffee_name");
const coffeeFilling = document.querySelector(".filling");
const buttons = document.querySelectorAll("[data-coffe]");
let currentElement = null;

buttons.forEach((button) => {
  button.addEventListener("click", (e) => {
    const changeCoffeeType = (button) => {
      
      if (currentElement) {
        currentElement.classList.remove("selected");
        coffeeFilling.classList.remove(currentElement.id);
      }
      currentElement = e.target;
      coffeeFilling.classList.add(currentElement.id);
      
      currentElement.classList.add("selected");
      coffeeName.innerText = button.innerText;
    };
    changeCoffeeType(button);
  });
});
	
/* Two 
-
-
*/	

var paths = document.querySelectorAll('#Mouse path, mask path'); // select all mouse and mask paths
var time = 18000; // how long the drawing should last in milliseconds
var totalLength = 0; // length of all paths
paths.forEach(function(path) {
    totalLength += path.getTotalLength(); // add length of all paths
});
var offset = 0; // how far each path is from the start
paths.forEach(function(path){
    var length = path.getTotalLength(); // length of current path
    path.style.setProperty('--length',length); // set css var for path length
    var duration = length / totalLength * time; // how long the path should take to draw - percentage of total time equal to the percentage of the path's length in the total length
    path.style.setProperty('--duration',duration+'ms'); // set css var for duration
    var delay = offset / totalLength * time; // delay path drawing until previous finished
    path.style.setProperty('--delay',delay+'ms'); // set css var for offset
    offset += length; // increase offset by current path's length
});


/* Four
-
-
-
*/

const inner = document.querySelector('.inner');
    const squareColors = ['#ed220d', '#b542e5', '#0d93ed', '#f2770b', '#8affbc', '#f2ff8a', '#ff38c2', '#e37f35'];
    const BLOCKS = 1344;
 
    for (let i = 0; i < BLOCKS; i++) {
      const square = document.createElement('div');
      square.classList.add('square');
      square.addEventListener('mouseover', () => setColor(square));
      square.addEventListener('mouseout', () => removeColor(square));
      inner.appendChild(square);
    }
    function setColor(elem) {
      const color = getRandomColor();
      elem.style.background = color;
      elem.style.boxShadow = `0 0 12px ${color}`;
    }
    function removeColor(elem) {
      elem.style.background = '#2d2d2d';
      elem.style.boxShadow = '0 0 4px #000';
    }
 
const getRandomColor = () => squareColors[Math.floor(Math.random() * squareColors.length)];















/* Three */



const {
  gsap: {
    registerPlugin,
    set,
    to,
    timeline,
    delayedCall,
    utils: { random },
  },
  MorphSVGPlugin,
  Draggable,
} = window;
registerPlugin(MorphSVGPlugin);

// Used to calculate distance of "tug"
let startX;
let startY;

const CORD_DURATION = 0.1;
const INPUT = document.querySelector('#light-mode');
const ARMS = document.querySelectorAll('.bear__arm');
const PAW = document.querySelector('.bear__paw');
const CORDS = document.querySelectorAll('.toggle-scene__cord');
const HIT = document.querySelector('.toggle-scene__hit-spot');
const DUMMY = document.querySelector('.toggle-scene__dummy-cord');
const DUMMY_CORD = document.querySelector('.toggle-scene__dummy-cord line');
const PROXY = document.createElement('div');
const endY = DUMMY_CORD.getAttribute('y2');
const endX = DUMMY_CORD.getAttribute('x2');
// set init position
const RESET = () => {
  set(PROXY, {
    x: endX,
    y: endY,
  });
};

const AUDIO = {
  BEAR_LONG: new Audio('https://assets.codepen.io/605876/bear-groan-long.mp3'),
  BEAR_SHORT: new Audio(
    'https://assets.codepen.io/605876/bear-groan-short.mp3'
  ),
  DOOR_OPEN: new Audio('https://assets.codepen.io/605876/door-open.mp3'),
  DOOR_CLOSE: new Audio('https://assets.codepen.io/605876/door-close.mp3'),
  CLICK: new Audio('https://assets.codepen.io/605876/click.mp3'),
};
const STATE = {
  ON: false,
  ANGER: 0,
};
set(PAW, {
  transformOrigin: '50% 50%',
  xPercent: -30,
});
set('.bulb', { z: 10 });
set(ARMS, {
  xPercent: 10,
  rotation: -90,
  transformOrigin: '100% 50%',
  yPercent: -2,
  display: 'block'
});
const CONFIG = {
  ARM_DUR: 0.4,
  CLENCH_DUR: 0.1,
  BEAR_START: 40,
  BEAR_FINISH: -55,
  BEAR_ROTATE: -50,
  DOOR_OPEN: 25,
  INTRO_DELAY: 1,
  BEAR_APPEARANCE: 2,
  SLAM: 3,
  BROWS: 4,
};
set('.bear__brows', { display: 'none' });
set('.bear', {
  rotate: CONFIG.BEAR_ROTATE,
  xPercent: CONFIG.BEAR_START,
  transformOrigin: '50% 50%',
  scale: 0,
  display: 'block',
});

RESET();

const CORD_TL = () => {
  const TL = timeline({
    paused: false,
    onStart: () => {
      // Hook this up to localStorage for jhey.dev
      STATE.ON = !STATE.ON;
      INPUT.checked = !STATE.ON;
      set(document.documentElement, { '--on': STATE.ON ? 1 : 0 });
      set([DUMMY], { display: 'none' });
      set(CORDS[0], { display: 'block' });
      AUDIO.CLICK.play();
    },
    onComplete: () => {
      // BEAR_TL.restart()
      set([DUMMY], { display: 'block' });
      set(CORDS[0], { display: 'none' });
      RESET();
    },
  });
  for (let i = 1; i < CORDS.length; i++) {
    TL.add(
      to(CORDS[0], {
        morphSVG: CORDS[i],
        duration: CORD_DURATION,
        repeat: 1,
        yoyo: true,
      })
    );
  }
  return TL;
};

/**
 * Mess around with the actial input toggling here.
 */
const BEAR_TL = () => {
  const ARM_SWING = STATE.ANGER > 4 ? 0.2 : CONFIG.ARM_DUR;
  const SLIDE = STATE.ANGER > CONFIG.BROWS + 3 ? 0.2 : random(0.2, 0.6);
  const CLOSE_DELAY = STATE.ANGER >= CONFIG.INTRO_DELAY ? random(0.2, 2) : 0;
  const TL = timeline({
    paused: false,
  })
    .to('.door', {
      onStart: () => AUDIO.DOOR_OPEN.play(),
      rotateY: 25,
      duration: 0.2,
    })
    .add(
      STATE.ANGER >= CONFIG.BEAR_APPEARANCE && Math.random() > 0.25
        ? to('.bear', {
            onStart: () => {
              if (Math.random() > 0.5) {
                // delayedCall(random(0, 1.5), () => {
                //   AUDIO[
                //     STATE.ANGER >= CONFIG.BROWS && Math.random() > 0.5
                //       ? 'BEAR_LONG'
                //       : 'BEAR_SHORT'
                //   ].play()
                // })
              }
              set('.bear', { scale: 1 });
            },
            xPercent: CONFIG.BEAR_FINISH,
            repeat: 1,
            repeatDelay: 1,
            yoyo: true,
            duration: SLIDE,
          })
        : () => {}
    )
    .to(ARMS, {
      delay: CLOSE_DELAY,
      duration: ARM_SWING,
      rotation: 0,
      xPercent: 0,
      yPercent: 0,
    })
    .to(
      [PAW, '#knuckles'],
      {
        duration: CONFIG.CLENCH_DUR,
        xPercent: (_, target) => (target.id === 'knuckles' ? 10 : 0),
      },
      `>-${ARM_SWING * 0.5}`
    )
    .to(ARMS, {
      duration: ARM_SWING * 0.5,
      rotation: 5,
    })
    .to(ARMS, {
      rotation: -90,
      xPercent: 10,
      duration: ARM_SWING,
      onComplete: () => {
        to('.door', {
          onComplete: () => AUDIO.DOOR_CLOSE.play(),
          duration: 0.2,
          rotateY: 0,
        });
      },
    })
    .to(
      DUMMY_CORD,
      {
        duration: CONFIG.CLENCH_DUR,
        attr: {
          x2: parseInt(endX, 10) + 20,
          y2: parseInt(endY, 10) + 60,
        },
      },
      '<'
    )
    .to(
      DUMMY_CORD,
      {
        duration: CONFIG.CLENCH_DUR,
        attr: {
          x2: endX,
          y2: endY,
        },
      },
      '>'
    )
    .to(
      [PAW, '#knuckles'],
      {
        duration: CONFIG.CLENCH_DUR,
        xPercent: (_, target) => (target.id === 'knuckles' ? 0 : -28),
      },
      '<'
    )
    .add(() => CORD_TL(), '<');
  return TL;
};

const IMPOSSIBLE_TL = () =>
  timeline({
    onStart: () => set(HIT, { display: 'none' }),
    onComplete: () => {
      set(HIT, { display: 'block' });
      if (Math.random() > 0) STATE.ANGER = STATE.ANGER + 1;
      if (STATE.ANGER >= CONFIG.BROWS) set('.bear__brows', { display: 'block' });
    },
  })
    .add(CORD_TL())
    .add(BEAR_TL());

Draggable.create(PROXY, {
  trigger: HIT,
  type: 'x,y',
  onPress: e => {
    startX = e.x;
    startY = e.y;
    RESET();
  },
  ondrag: function() {
    set(DUMMY_CORD, {
      attr: {
        x2: this.x,
        y2: this.y,
      },
    });
  },
  onRelease: function(e) {
    const DISTX = Math.abs(e.x - startX);
    const DISTY = Math.abs(e.y - startY);
    const TRAVELLED = Math.sqrt(DISTX * DISTX + DISTY * DISTY);
    to(DUMMY_CORD, {
      attr: { x2: endX, y2: endY },
      duration: CORD_DURATION,
      onComplete: () => {
        if (TRAVELLED > 50) {
          IMPOSSIBLE_TL();
        } else {
          RESET();
        }
      },
    });
  },
});
